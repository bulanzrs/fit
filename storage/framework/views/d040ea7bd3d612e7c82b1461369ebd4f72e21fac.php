<?php $__env->startSection('head'); ?>
<link href="/css/app/register.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/jquery.steps.js<?php echo e(config('app.link_version')); ?>"></script>
<script type="text/javascript" src="/js/app/register.js<?php echo e(config('app.link_version')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>



<?php echo Form::open(['route' => 'register' , 'class' => "register-form"]); ?>

<div id="register-step">
    <h2>1</h2>
    <section>
        <h1>What's your <b>GOAL</b></h1>
        <div class='inputGroup-section'>
            <div class="inputGroup">
                <input id="radio1" name="goal" type="radio"  value="0"/>
                <label for="radio1">No Goal</label>
            </div>
            <div class="inputGroup">
                <input id="radio2" name="goal" type="radio"  value="1"/>
                <label for="radio2">Lose Weight</label>
            </div>
            <div class="inputGroup">
                <input id="radio3" name="goal" type="radio"  value="2"/>
                <label for="radio3">Keep Fit</label>
            </div>
            <div class="inputGroup">
                <input id="radio4" name="goal" type="radio" value="3"/>
                <label for="radio4">Meet More Friends</label>
            </div>
        </div>
    </section>

    <h2>2</h2>
    <section>
        <h1>How much do you weigh (KG)</h1>
        <input type='text' class='form-control theme-input' name='weight' placeholder='60'/>
    </section>

    <h2>3</h2>
    <section>
        <h1>How tall are you</h1>
        <input type='text' class='form-control theme-input' name='height' placeholder='60'/>
    </section>

    <h2>4</h2>
    <section>
        <h1>Who are you</h1>
        <b>Name </b>
        <input type='text' class='form-control' name='name'/>
        <br>
        <b>Age </b>
        <input type='text' class='form-control' name='age'/>
        <br>
        <b>Gender </b>
        <div class="option-group">
            <div class="option-container">
                <input class="option-input" checked id="option-1" type="radio" name="gender" value='0'/>
                <input class="option-input" id="option-2" type="radio" name="gender" value='1'/>
                <label class="option" for="option-1"  value='1' >
                    <span class="option__indicator"></span>
                    <span class="option__label">
                    <sub>Female</sub>
                    </span>
                </label>
                <label class="option" for="option-2">
                    <span class="option__indicator"></span>
                    <span class="option__label">
                    <sub>Male</sub>
                    </span>
                </label>
            </div>
        </div>

    </section>

    <h2>5</h2>
    <section>
        <h1>What is account details</h1>
        <b>Email </b>
        <input type='text' class='form-control' name='email'/>
        <br>
        <b>Password </b>
        <input type='password' class='form-control' name='pass'/>
        <br>
        <b>Confirm Password </b>
        <input type='password' class='form-control' name='conf-pass'/>
    </section>
</div>

<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>

<?php echo $__env->make("app.layout.auth", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>