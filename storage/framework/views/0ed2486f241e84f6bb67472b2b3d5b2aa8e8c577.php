<?php $__env->startSection('head'); ?>
<link href="/css/app/group.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/group.js<?php echo e(config('app.link_version')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class='gap'>
        <?php if($group->isOwner()): ?>
            <button class='btn btn-default' data-toggle="modal" data-target="#createPostModal"> Create Post </button>
            <button class='btn btn-default' data-toggle="modal" data-target="#manageGroupModal"> Manage Group </button>
            <button class='btn btn-default' data-toggle="modal" data-target="#manageGamificationModal"> Manage Gamification </button>

        <?php endif; ?>

        <div class='post-list' style="margin-top: 15px">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class='post'>
                    <div class="card">
                        <div class="card-body">
                            <h4 class='title'> <?php echo e($post->content); ?></h4>

                            <small class='author'>
                                <i class="fa fa-pencil" style="color: #0BD2FD"></i>&nbsp;<b><?php echo e($post->getOwner()); ?></b>
                            </small>
                            <br>
                            <small  class='date'>Published on &nbsp; <?php echo e($post->created_at); ?></small >
                        </div>
                    </div>
                    <br>

                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php echo $__env->make('app.modal.post', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('app.modal.group', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("app.layout.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>