<?php $__env->startSection('head'); ?>
<link href="/css/app/group.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/group.js<?php echo e(config('app.link_version')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class='post'>
            <div class="card" style="margin: 40px">
                <div class="card-body">
                    <h4 class='name' style="color: #5DADE2"> <?php echo e($group->name); ?></h4>
                    <small class='description'> <?php echo e($group->description); ?></small>

                    <?php if(!$group->hasUser()): ?>
                        <?php echo Form::open(['route' => 'app.group.join' ]); ?>

                        <input type='hidden' value="<?php echo e($group->id); ?>" name='groupID'/>
                        <button class="btn btn-outline-success btn-sm" style="margin-top: 10px"><i class="fa fa-handshake-o" aria-hidden="true"></i> Join Group </button>
                        <?php echo Form::close(); ?>

                    <?php endif; ?>
                </div>
            </div>
            <br>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("app.layout.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>