<?php $__env->startSection('head'); ?>
<link href="/css/app/login.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/login.js<?php echo e(config('app.link_version')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <!-- login backround -->
    <div class='login-bg'>
    </div>

    <!-- Content Panel -->
    <div class="login-page">
        <div class="form">
            <form method="POST" action="<?php echo e(route('login')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                 <?php echo csrf_field(); ?>
                 <img src='/img/logo/logo_white.png' class='logo'/>
                 <input type="text" name="email" placeholder="Username" class='userID form-control' required/>
                 <input type="password" name="password" placeholder="Password" class='pass form-control '  required/>
                 <button type='submit' name="login" class='login-btn btn white-theme-btn'>Login</button>
                 <a href='/'>
                     <button type='button' class=' btn back-btn'>Back</button>
                 </a>
            </form>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("app.layout.auth", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>