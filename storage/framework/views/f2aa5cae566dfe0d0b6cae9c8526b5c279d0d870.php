<?php $__env->startSection('head'); ?>
<link href="/css/plugin/slick.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
<link href="/css/plugin/slick-theme.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
<link href="/css/app/group.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/slick.min.js<?php echo e(config('app.link_version')); ?>"></script>
<script type="text/javascript" src="/js/app/group.js<?php echo e(config('app.link_version')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class='top-section'>
    <button class='btn btn-link add-btn' data-toggle="modal" data-target="#addGroupModal">
        <img  class='add-icon' src='/img/icon/add.png'/>
    </button>

</div>

<div class='row search-section'>
    <div class='col-12'>
        <input type='text' class='form-control' placeholder='Search group name, activity, tag ... '/>
    </div>
</div>

<div class='group-slider'>
    <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class='group-item'>
        <img src="<?php echo e($group->image_url); ?>"/>
        <p><?php echo e($group->name); ?></p>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<div class='row content-section'>
    <div class='col-12 no-group'>
        <h1> You don't have any group yet </h1>
        <button class='btn btn-default'> Create Group or Join Group </button>
    </div>
</div>


<?php echo $__env->make('app.modal.group', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make("app.layout.app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>