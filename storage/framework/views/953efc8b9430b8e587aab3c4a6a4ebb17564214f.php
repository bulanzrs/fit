
<!-- Add gorup modal -->
<div id="addGroupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <?php echo Form::open(['route' => 'app.group.create' , 'class' => "group-form"]); ?>

                <div class="modal-header">
                    <h4 class="modal-title">
                        Create group
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <p>Name </p>
                        <input class='form-control' type='text' name='name'/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Description </p>
                        <input class='form-control' type='text' name='description'/>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Type of Activity</p>
                        <select class='form-control' name='activity'>
                            <option value='0'> Running </option>
                            <option value='1'> Cycling </option>
                            <option value='2'> Others </option>
                        </select>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Privacy </p>
                        <select class='form-control' name='privacy'>
                            <option value='0'> Public </option>
                            <option value='1'> Private </option>
                            <option value='2'> Secret </option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php echo Form::submit("Create",['class'=>'btn btn-success']); ?>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
