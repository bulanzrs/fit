<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">

<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <title><?php echo e(env('APP_NAME')); ?></title>
    <!-- Meta End-->

    <!-- Styles -->
    <link rel="shortcut icon" href='/img/logo/logo.ico'/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,400,700,900" rel="stylesheet">
    <link href="/css/plugin/bootstrap.min.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/normalize.min.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/fontawesome.min.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/themify-icons.min.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/css/plugin/open-iconic.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugin/tooltipster.bundle.min.css" />
    <link href="/css/app/main_auth.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
    <link href="/css/app/responsive_auth.css<?php echo e(config('app.link_version')); ?>" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/jquery.min.js<?php echo e(config('app.link_version')); ?>"></script>
    <script type="text/javascript" src="/js/plugin/tooltipster.bundle.min.js<?php echo e(config('app.link_version')); ?>"></script>
    <script type="text/javascript" src="/js/plugin/bootstrap.min.js<?php echo e(config('app.link_version')); ?>"></script>
    <script type="text/javascript" src="/js/plugin/sweetalert.min.js<?php echo e(config('app.link_version')); ?>"></script>
    <script type="text/javascript" src="/js/plugin/foundation.js<?php echo e(config('app.link_version')); ?>"></script>
    <script type="text/javascript" src="/js/app/main_auth.js<?php echo e(config('app.link_version')); ?>"></script>
    <!-- Script End -->

    <?php echo $__env->yieldContent('head'); ?>

</head>

<body>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/loader.gif'/>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Body Content -->
    <div class="content">
        <?php echo $__env->yieldContent('content'); ?>
    </div>
    <!-- Body Content End-->

</body>

<!-- Script to handle notification -->
<script>
    <?php if(Session::has('success')): ?>
        swal('',"<?php echo e(Session::get('success')); ?>",'success');
    <?php endif; ?>
    <?php if(Session::has('err')): ?>
        swal('',"<?php echo e(Session::get('err')); ?>",'warning');
    <?php endif; ?>
</script>

</html>
