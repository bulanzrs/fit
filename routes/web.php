<?php


//Check if is dashboard portal
if(env('IS_DASHBOARD'))
{

    //Authentication\
    $this->get('/', 'Dashboard\AuthController@showLoginForm')->name('login');
    $this->post('/', 'Dashboard\AuthController@login');

    //Dashboard Route
    Route::group(['prefix'=>'dashboard','as'=>'dashboard.', 'namespace'=>'Dashboard','middleware'=>['auth']],function () {

        //Logout
        Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'AuthController@logout']);

        //Index page
        Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

        //user page
        Route::get('/user', ['as' => 'user', 'uses' => 'UserController@index']);

        //Avatar route
        Route::group(['as'=>'avatar.'],function () {
            Route::get('/avatar', ['as' => 'index', 'uses' => 'AvatarController@index']);
            Route::post('/avatar', ['as' => 'create', 'uses' => 'AvatarController@create']);
            Route::put('/avatar', ['as' => 'update', 'uses' => 'AvatarController@update']);
            Route::delete('/avatar', ['as' => 'delete', 'uses' => 'AvatarController@delete']);
        });

        //Badge route
        Route::group(['as'=>'badge.'],function () {
            Route::get('/badge', ['as' => 'index', 'uses' => 'BadgeController@index']);
            Route::post('/badge', ['as' => 'create', 'uses' => 'BadgeController@create']);
            Route::put('/badge', ['as' => 'update', 'uses' => 'BadgeController@update']);
            Route::delete('/badge', ['as' => 'delete', 'uses' => 'BadgeController@delete']);
        });

        //Quest route
        Route::group(['as'=>'quest.'],function () {
            Route::get('/quest', ['as' => 'index', 'uses' => 'QuestController@index']);
            Route::post('/quest', ['as' => 'create', 'uses' => 'QuestController@create']);
            Route::put('/quest', ['as' => 'update', 'uses' => 'QuestController@update']);
            Route::delete('/quest', ['as' => 'delete', 'uses' => 'QuestController@delete']);
        });

    });

}
else
{

    //Authentication
    $this->get('/login', 'App\AuthController@showLoginForm')->name('login');
    $this->post('/login', 'App\AuthController@login');
    Route::get('/register', ['as' => 'register', 'uses' => 'App\AuthController@showRegisterForm']);
    Route::post('/register', ['as' => 'register', 'uses' => 'App\AuthController@register']);

    //Landing page / Homepage
    Route::get('/', ['as' => 'app.info', 'uses' => 'App\IndexController@index']);

    //Website Route
    Route::group(['as'=>'app.', 'namespace'=>'App','middleware'=>['auth']],function () {

        //Logout
        Route::get('/logout', ['as' => 'user.logout', 'uses' => 'AuthController@logout']);

        //Group route
        Route::group(['prefix'=>'group', 'as'=>'group.'],function () {
            Route::get('/', ['as' => 'index', 'uses' => 'GroupController@index']);
            Route::get('/{id}', ['as' => 'index', 'uses' => 'GroupController@show']);
            Route::get('/search', ['as' => 'index', 'uses' => 'GroupController@search']);
            Route::post('/create', ['as' => 'create', 'uses' => 'GroupController@create']);
            Route::post('/search', ['as' => 'search', 'uses' => 'GroupController@search']);
            Route::post('/join', ['as' => 'join', 'uses' => 'GroupController@join']);
            Route::put('/update', ['as' => 'update', 'uses' => 'GroupController@update']);
            Route::delete('/delete', ['as' => 'delete', 'uses' => 'GroupController@delete']);
        });

        //Post route
        Route::group(['prefix'=>'post', 'as'=>'post.'],function () {
            Route::post('/create', ['as' => 'create', 'uses' => 'PostController@createPost']);
            Route::put('/update', ['as' => 'update', 'uses' => 'GroupController@update']);
            Route::delete('/delete', ['as' => 'delete', 'uses' => 'GroupController@delete']);
        });

        //Profile page
        Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);

        //Achievement page
        Route::get('/achievement', ['as' => 'achievement', 'uses' => 'AchievementController@index']);

        //Reward page
        Route::get('/reward', ['as' => 'reward', 'uses' => 'RewardController@index']);

    });

}
