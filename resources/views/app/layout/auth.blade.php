<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{env('APP_NAME')}}</title>
    <!-- Meta End-->

    <!-- Styles -->
    <link rel="shortcut icon" href='/img/logo/logo.ico'/>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,400,700,900" rel="stylesheet">
    <link href="/css/plugin/bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/normalize.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/themify-icons.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/css/plugin/open-iconic.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugin/tooltipster.bundle.min.css" />
    <link href="/css/app/main_auth.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/app/responsive_auth.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/tooltipster.bundle.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/sweetalert.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/foundation.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/app/main_auth.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

    @yield('head')

</head>

<body>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/loader.gif'/>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Body Content -->
    <div class="content">
        @yield('content')
    </div>
    <!-- Body Content End-->

</body>

<!-- Script to handle notification -->
<script>
    @if (Session::has('success'))
        swal('',"{{Session::get('success')}}",'success');
    @endif
    @if (Session::has('err'))
        swal('',"{{Session::get('err')}}",'warning');
    @endif
</script>

</html>
