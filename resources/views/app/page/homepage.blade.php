@extends("app.layout.app")

@section('head')
<link href="/css/plugin/slick.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/plugin/slick-theme.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/homepage.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/slick.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/homepage.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='row'>


    <div class='col-9 gap'>
        @if(Auth::user()->hasGroup())
            <div class='row '>
                <div class='col-10 manage-group'>
                    <small> Group List </small>
                    @foreach(Auth::user()->groups as $group)

                        <div class="card">
                            <div class="card-body">
                                <a href='/group/{{$group->reference_name}}'>
                                    <p> {{$group->name}} </p>
                                </a>
                                <small><i class='ti-user'> </i> 100 Join this Group </small>
                            </div>
                        </div>
                        <br>
                    @endforeach
                </div>
            </div>
        @else
            <div class='row content-section'>
                <div class='col-12 no-group'>
                    <h1> You haven't join any group yet </h1>
                    <button class='btn btn-default'> Join now or create your own  ! </button>
                </div>
            </div>
        @endif
    </div>
    <div class='col-3'>
        <div class='row '>
            <div class='col-12 no-group right-section'>
                <img src='/img/icon/profile.png' class='profile_pic'/>
                <div class='details'>
                    <p class='name'>{{Auth::user()->name}} </p>
                    <p class='level'>Level {{Auth::user()->level}} </p>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: {{Auth::user()->getExp()}}%"></div>
                </div>
                <p class='exp'>{{Auth::user()->exp}} EXP </p>

                <div class='content-section'>
                    <p class='badge-label'> Your don't have any badge </p>
                </div>
                <div class='content-section'>
                    <p class='badge-label'> Your don't have any avatar </p>
                </div>

            </div>
        </div>
    </div>
</div>





<!--
    <div class='top-bg'>
        <img src='/img/icon/cloud.png' class='cloud1'/>
        <img src='/img/icon/cloud.png' class='cloud2'/>
        <img src='/img/icon/cloud3.png' class='cloud3'/>
        <img src='/img/icon/cloud3.png' class='cloud4'/>
    </div>
    <div class='top-section'>
        <h1> Current Goal </h1>
        <p> Lose Weight </p>
    </div>
    <div class='group-section'>
        <h1> Recent Group </h1>
        <div class='group-slider'>
            @foreach($groups as $group)
            <div class='group-item'>
                <img src="{{$group->image_url}}"/>
                <p>{{$group->name}}</p>
            </div>
            @endforeach
        </div>
    </div>
    <div class='row content-section'>
        <div class='col-12 no-group'>
            <h1> You din't get any badge yet </h1>
            <button class='btn btn-default'> Get your first badge ! </button>
        </div>
    </div>
    <div class='row content-section'>
        <div class='col-12 no-group'>
            <h1> You don't any quest yet </h1>
            <button class='btn btn-default'> Find your first quest! </button>
        </div>
    </div>
-->


@stop
