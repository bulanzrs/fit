@extends("app.layout.auth")

@section('head')
    <link href="/css/plugin/slick.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/slick-theme.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/app/landing.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="/js/plugin/slick.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/app/landing.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

    <div class='slider-section'>
        <div class='slider-item'>
            <img src='/img/picture/landing-1.png'/>
            <p>Welcome to {{env('APP_NAME')}}</p>
            <p> We make exercise fun. </p>
        </div>
        <div class='slider-item'>
            <img src='/img/picture/target.png'/>
            <p>Set your fitness goal</p>
        </div>
        <div class='slider-item'>
            <img src='/img/picture/record.png'/>
            <p> Record your exercise activity</p>
            <p> Bind with gadget or track your exercise log </p>
        </div>
        <div class='slider-item'>
            <img src='/img/picture/game.png'/>
            <p>Fun way to exercise</p>
            <p>Unlockable avatar, challenging quest, interesting reward and competition </p>
        </div>
        <div class='slider-item'>
            <img src='/img/picture/goal.png'/>
            <p>Play and achieve your goal</p>
        </div>

    </div>
    <div class='login-section'>

        <a href='/register'>
            <button class='btn btn-default theme-btn'> Get Started </button>  </a>
        <br>
        <a href='/login'>
            <button class='btn btn-default second-theme-btn '>  Login </button>
        </a>

    </div>

@stop
