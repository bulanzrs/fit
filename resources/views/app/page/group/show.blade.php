@extends("app.layout.app")

@section('head')
<link href="/css/app/group.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/group.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
    <div class='gap'>
        @if($group->isOwner())
            <button class='btn btn-default' data-toggle="modal" data-target="#createPostModal"> Create Post </button>
            <button class='btn btn-default' data-toggle="modal" data-target="#manageGroupModal"> Manage Group </button>
            <button class='btn btn-default' data-toggle="modal" data-target="#manageGamificationModal"> Manage Gamification </button>

        @endif

        <div class='post-list' style="margin-top: 15px">
            @foreach($posts as $post)
                <div class='post'>
                    <div class="card">
                        <div class="card-body">
                            <h4 class='title'> {{$post->content}}</h4>

                            <small class='author'>
                                <i class="fa fa-pencil" style="color: #0BD2FD"></i>&nbsp;<b>{{$post->getOwner()}}</b>
                            </small>
                            <br>
                            <small  class='date'>Published on &nbsp; {{$post->created_at}}</small >
                        </div>
                    </div>
                    <br>

                </div>
            @endforeach
        </div>
    </div>
@include('app.modal.post')
@include('app.modal.group')

@stop
