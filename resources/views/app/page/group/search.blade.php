@extends("app.layout.app")

@section('head')
<link href="/css/app/group.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/group.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
    @foreach($groups as $group)
        <div class='post'>
            <div class="card" style="margin: 40px">
                <div class="card-body">
                    <h4 class='name' style="color: #5DADE2"> {{$group->name}}</h4>
                    <small class='description'> {{$group->description}}</small>

                    @if(!$group->hasUser())
                        {!! Form::open(['route' => 'app.group.join' ]) !!}
                        <input type='hidden' value="{{$group->id}}" name='groupID'/>
                        <button class="btn btn-outline-success btn-sm" style="margin-top: 10px"><i class="fa fa-handshake-o" aria-hidden="true"></i> Join Group </button>
                        {!! Form::close() !!}
                    @endif
                </div>
            </div>
            <br>
        </div>
    @endforeach
@stop
