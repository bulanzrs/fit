@extends("app.layout.app")

@section('head')
<link href="/css/plugin/slick.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/plugin/slick-theme.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/app/group.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/slick.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/app/group.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='top-section'>
    <button class='btn btn-link add-btn' data-toggle="modal" data-target="#addGroupModal">
        <img  class='add-icon' src='/img/icon/add.png'/>
    </button>

</div>

<div class='row search-section'>
    <div class='col-12'>
        <input type='text' class='form-control' placeholder='Search group name, activity, tag ... '/>
    </div>
</div>

<div class='group-slider'>
    @foreach($groups as $group)
    <div class='group-item'>
        <img src="{{$group->image_url}}"/>
        <p>{{$group->name}}</p>
    </div>
    @endforeach
</div>

<div class='row content-section'>
    <div class='col-12 no-group'>
        <h1> You don't have any group yet </h1>
        <button class='btn btn-default'> Create Group or Join Group </button>
    </div>
</div>


@include('app.modal.group')

@stop
