@extends("app.layout.auth")

@section('head')
<link href="/css/app/login.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/app/login.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

    <!-- login backround -->
    <div class='login-bg'>
    </div>

    <!-- Content Panel -->
    <div class="login-page">
        <div class="form">
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                 @csrf
                 <img src='/img/logo/logo_white.png' class='logo'/>
                 <input type="text" name="email" placeholder="Username" class='userID form-control' required/>
                 <input type="password" name="password" placeholder="Password" class='pass form-control '  required/>
                 <button type='submit' name="login" class='login-btn btn white-theme-btn'>Login</button>
                 <a href='/'>
                     <button type='button' class=' btn back-btn'>Back</button>
                 </a>
            </form>
        </div>
    </div>

@stop
