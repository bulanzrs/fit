
<!-- Create post modal -->
<div id="createPostModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['route' => 'app.post.create' , 'class' => "group-form"]) !!}
                <input type='hidden' name='reference_name' value="{{$id}}"/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        Create post
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <p>Content </p>
                        <input class='form-control' type='text' name='content'/>
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit("Create",['class'=>'btn btn-success']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
