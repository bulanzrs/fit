
<div id="addBadgeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route' => 'dashboard.badge.create','files'=>'true', 'id' =>'file-upload-form' ,'class'=>'uploader']) !!}
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class='ti-star icon-purple'> </i>
                        Add Badge
                    </h4>
                </div>
                <div class="modal-body row">
                    <div class='col-sm-12 form-group'>
                        <p class='required'>Upload Badge Picture</p>
                        <input id="file-upload" type="file" name="badge"/>
                        <label for="file-upload" id="file-drag">
                            <div id="start">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                <div>{{trans('lang.select_or_drag')}}</div>
                                <div id="notimage" class="hidden">{{trans('lang.uploaded')}}</div>
                                <span id="file-upload-btn" class="uploader-btn btn btn-primary">{{trans('lang.select_file')}}</span>
                            </div>
                            <div id="response" class="hidden">
                                <div id="messages"></div>
                                <progress class="progress" id="file-progress" value="0">
                                    <span>0</span>%
                                </progress>
                            </div>
                        </label>
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p class='required'>Title</p>
                        <input type="text" class="form-control" name="title">
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Description</p>
                        <input type="text" class="form-control" name="description">
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p class=''>Condition</p>
                          {{ Form::select('condition_id', [''=>'No Condition']+$conditionArr, null , ['class' => 'form-control ']) }}
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p class=''>Condition Value</p>
                        <input type="text" class="form-control" name="condition_value">
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p class=''>Reward</p>
                          {{ Form::select('reward_id', [''=>'No Reward']+$rewardArr, null , ['class' => 'form-control ']) }}
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p class=''>Reward Value</p>
                        <input type="text" class="form-control" name="reward_value">
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit(trans('lang.create'),['class'=>'btn btn-success']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('lang.cancel')}}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



<div id="editAvatarModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route' => 'dashboard.avatar.update','method'=>'PUT']) !!}
                <input type='hidden' name='editID' class='edit_id'/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class='ti-marker icon-blue'> </i>
                        Update Avatar
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <p>{{trans('lang.card_number')}} </p>
                        <input type="text" class="form-control edit-card" name="card">
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit(trans('lang.update'),['class'=>'btn btn-success']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('lang.cancel')}}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<div id="deleteAvatarModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route' => 'dashboard.avatar.delete','method'=>'DELETE']) !!}
                <input name='deleteID' class='delete_id' type='hidden'/>
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class='ti-trash icon-red'> </i>
                        Delete Avatar
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        {{trans('lang.are_you_sure')}}
                    </div>

                </div>
                <div class="modal-footer">
                    {!! Form::submit(trans('lang.delete'),['class'=>'btn btn-danger']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('lang.cancel')}}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
