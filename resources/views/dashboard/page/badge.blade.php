@extends("dashboard.page.layout")

@section('head')
    <link href="/css/plugin/uploadBox.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/component/modal.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/component/table.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/avatar.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/flatpickr.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="/js/plugin/flatpickr.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/uploadBox.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/dashboard/component/table.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<button class='btn btn-link add-btn' data-toggle="modal" data-target="#addBadgeModal">
    <i class='ti-plus'> </i>
</button>

<div class='table-section'>
    <div class='inline-table-form-section'>
        <i class='ti-control-record icon-red'> </i>
        <h3 class='title'>Badge Management</h3>

        {{ Form::open(array('method' =>'GET')) }}
        <div class="form-group row">
            <label for="example-week-input" class="col-12 col-sm-2 col-form-label">{{trans('lang.search')}}</label>
            <div class="col-12 col-sm-10">
                <input class="form-control" type="text" name='query' placeholder="Search query here ..." value='{{$query}}'>
            </div>
        </div>
        <button class='btn btn-primary'> {{trans('lang.search')}} </button>
        {{ Form::close() }}
    </div>
    <p class='subtitle'>{{trans('lang.hold_shift')}} </p>
    <div class="table-responsive">
        <table class="table">
            <thead class='thead-blue'>
                <tr>
                    <th scope="col">Badge</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Condition</th>
                    <th scope="col">Condition Value</th>
                    <th scope="col">Reward</th>
                    <th scope="col">Reward Value</th>
                    <th scope="col">{{trans('lang.date')}}</th>
                    <th scope="col">{{trans('lang.action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($records as $result)
                    <tr>
                        <td>
                            <img src='/uploads/{{$result->image_url}}' class='avatar_img'/>
                        </td>
                        <td>{{$result->title}}</td>
                        <td>{{$result->description}}</td>
                        <td>{{($con=$result->conditions)?$con->name:"No Condition"}}</td>
                        <td>{{$result->condition_value}}</td>
                        <td>{{($con=$result->reward)?$con->name:"No Reward"}}</td>
                        <td>{{$result->reward_value}}</td>
                        <td>{{$result->created_at}}</td>
                        <td>
                            <button  data-toggle="modal" data-target="#editCardModal" class='btn btn-default edit-btn' value="{{$result->id}}">
                                {{trans('lang.edit')}}
                            </button>
                            <button  data-toggle="modal" data-target="#deleteCardModal" class='btn btn-danger delete-btn' value="{{$result->id}}">
                                {{trans('lang.delete')}}
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $records->appends(['query' => $query])->links() }}
        <p class='search-total'> {{$records->count()}} {{trans('lang.records')}}</p>
    </div>
</div>
@include('dashboard.modal.Badge')
@stop
