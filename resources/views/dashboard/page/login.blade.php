<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{env('APP_NAME')}} Dashboard</title>
    <!-- Meta End-->

    <!-- Styles -->
    <link rel="shortcut icon" href="/img/logo/logo.ico"/>
    <link href="/css/plugin/bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/animate.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/animation.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/login.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/sweetalert.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/dashboard/login.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

</head>

<body>

    <!-- Laoder section -->
    <div class='loader'>
        <div class='loading-section'>
            <img src="/img/icon/loader.gif"/>
        </div>
    </div>

    <!-- Content Panel -->

    <div id="particles-background" class="vertical-centered-box"></div>
    <div id="particles-foreground" class="vertical-centered-box"></div>

    <div class="login-page">
        <div class="form">
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                 @csrf
                 <img src='/img/logo/logo_white.png' class='logo'/>
                 <input type="text" name="email" placeholder="Username" class='userID' required/>
                 <input type="password" name="password" placeholder="Password" class='pass'  required/>
                 <div class='captcha'>
                      @captcha
                  </div>
                  <input type="text" name="captcha" placeholder="Captcha" class='captcha'  required/>
                 <button type='submit' name="login" class='login-btn'>Login</button>
            </form>
        </div>
    </div>

</body>

<!-- script to handle error -->

<script>
    @if ($errors->has('email') || $errors->has('password'))
        swal('',"{{trans('lang.login_invalid')}}",'warning');
    @endif
    @if ($errors->has('captcha'))
        swal('',"{{trans('lang.captcha_error')}}",'warning');
    @endif
    @if (Session::has('success'))
        swal('',"{{Session::get('success')}}",'success');
    @endif
    @if (Session::has('err'))
        swal('',"{{Session::get('err')}}",'warning');
    @endif
</script>
<script type="text/javascript" src="/js/plugin/particle.min.js{{ config('app.link_version') }}"></script>

</html>
