<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{env('APP_NAME')}} Dashboard</title>
    <!-- Meta End-->

    <!-- Style setting -->
    @include('dashboard.setting.theme')
    <!-- Style setting end -->

    <!-- Styles -->
    <link rel="shortcut icon" href='/img/logo/logo.ico'/>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
    <link href="/css/plugin/bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/normalize.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/themify-icons.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/css/plugin/open-iconic.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugin/tooltipster.bundle.min.css" />
    <link href="/css/dashboard/foundation.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/main.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/responsive.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/tooltipster.bundle.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>

    <script type="text/javascript" src="/js/plugin/sweetalert.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/foundation.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/dashboard/main.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

    @yield('head')

</head>

<body>

    <div class='bg-color'></div>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/loader.gif'/>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Menu Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src='/img/logo/logo_white.png' alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src='/img/logo/logo_white.png'  alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="{{ ((Request::route()->getName()=='dashboard.index')?'active':' ')}}">
                        <a href='/'>
                            <i class="menu-icon icon-red ti-home"></i>
                            {{trans('lang.dashboard')}}
                        </a>
                    </li>
                    <li class="{{ ((Request::route()->getName()=='dashboard.user')?'active':' ')}}">
                        <a href='/dashboard/user'>
                            <i class="menu-icon icon-purple ti-user"></i>
                            {{trans('lang.user_management')}}
                        </a>
                    </li>
                    <li class="{{ ((Request::route()->getName()=='dashboard.avatar.index')?'active':' ')}}">
                        <a href='/dashboard/avatar'>
                            <i class="menu-icon icon-lightred ti-comments-smiley"></i>
                            Avatar Management
                        </a>
                    </li>
                    <li class="{{ ((Request::route()->getName()=='dashboard.badge.index')?'active':' ')}}">
                        <a href='/dashboard/badge'>
                            <i class="menu-icon icon-blue ti-star"></i>
                            Badge Management
                        </a>
                    </li>
                    <li class="{{ ((Request::route()->getName()=='dashboard.quest.index')?'active':' ')}}">
                        <a href='/dashboard/quest'>
                            <i class="menu-icon icon-green ti-bookmark"></i>
                            Quest Management
                        </a>
                    </li>
                    <!--
                    <li class="{{ ((Request::route()->getName()=='dashboard.reward')?'active':' ')}}">
                        <a href='/dashboard/achievement'>
                            <i class="menu-icon icon-lightblue ti-cup"></i>
                            Redeem Management
                        </a>
                    </li>
                    <li class="{{ ((Request::route()->getName()=='dashboard.achievement')?'active':' ')}}">
                        <a href='/dashboard/achievement'>
                            <i class="menu-icon icon-green ti-shine"></i>
                            Plant Management
                        </a>
                    </li>

                    <li class="{{ ((Request::route()->getName()=='dashboard.achievement')?'active':' ')}}">
                        <a href='/dashboard/achievement'>
                            <i class="menu-icon icon-blue ti-star"></i>
                            Goal Management
                        </a>
                    </li>
                    <li class="{{ ((Request::route()->getName()=='dashboard.achievement')?'active':' ')}}">
                        <a href='/dashboard/achievement'>
                            <i class="menu-icon icon-blue ti-star"></i>
                            Food Management
                        </a>
                    </li>
                    <li class="{{ ((Request::route()->getName()=='dashboard.achievement')?'active':' ')}}">
                        <a href='/dashboard/achievement'>
                            <i class="menu-icon icon-blue ti-star"></i>
                            Event Management
                        </a>
                    </li>
                    -->
                    <li class="{{ ((Request::route()->getName()=='dashboard.achievement')?'active':' ')}}">
                        <a href='/dashboard/achievement'>
                            <i class="menu-icon icon-red ti-crown"></i>
                            Guild Management
                        </a>
                    </li>
                    <hr>
                    <li class="{{ ((Request::route()->getName()=='dashboard.logout')?'active':' ')}}">
                        <a href='/dashboard/logout'>
                            <i class="menu-icon ti-shift-left"></i>
                            {{trans('lang.logout')}}
                        </a>
                    </li>
                </ul>

            </div>
        </nav>
    </aside>
    <!-- Menu Panel End-->

    <!-- Content Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header -->
        <header id="header" class="header">
            <div class="header-menu">

                <!-- Searchbar -->
                <div class="col-sm-6">
                <!--     <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a> -->
                </div>
                <!-- Searchbar End -->

                <div class="col-sm-6 user-col">
                    <!-- Profile -->
                    <div class="user-area dropdown float-right">
                        <div class='col-sm-12'>
                            <a href="#" class="" aria-haspopup="true" aria-expanded="false" >
                                <img class="user-avatar rounded-circle profile-tooltip" src="/img/icon/profile.png" alt="User Avatar"  data-tooltip-content="#tooltip_content">
                            </a>
                            <div class="tooltip_templates">
                                <span id="tooltip_content">
                                    <a href="/dashboard/logout"> <i class="menu-icon icon-lightred ti-share"></i>{{trans('lang.logout')}}  </a>
                                </span>
                            </div>
                            <label class='username'> {{Auth::user()->name}}  </label>
                        </div>
                    </div>
                    <!-- Profile End -->

                    <!-- Language  -->
                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>
                    <!-- Language End -->

                </div>
            </div>
        </header>
        <!-- Header End-->

        <!-- Body Content -->
        <div class="content mt-3">
            @if(env('DASHBOARD_BG_PICTURE'))
                <img class='dashboard-bg' src="/img/picture/{{env('DASHBOARD_BG_PICTURE')}}.png"/>
            @endif
            @yield('content')
        </div>
        <!-- Body Content End-->

    </div>
    <!-- Content Panel End-->

</body>
<script>
    @if (Session::has('success'))
        swal('',"{{Session::get('success')}}",'success');
    @endif
    @if (Session::has('err'))
        swal('',"{{Session::get('err')}}",'warning');
    @endif
</script>
</html>
