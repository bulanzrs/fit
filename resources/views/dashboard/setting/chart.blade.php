

<script>
var ctx_card = document.getElementById("userChart");
var cardChart = new Chart(ctx_card, {
    type: 'line',
    maintainAspectRatio: false,
    intersect: true,
    data: {
        labels: [
            @foreach($dailyUser as $date)
                "{{$date->date}}",
            @endforeach
        ],
        datasets: [{
            label: "Daily Registered User" ,
            data: [
                @foreach($dailyUser as $load)
                    "{{$load->trans}}",
                @endforeach
            ],
            backgroundColor: [
                @foreach($dailyUser as $load)
                    'rgba(255, 99, 132, 0.2)',
                @endforeach
            ],
            borderColor: [
                @foreach($dailyUser as $load)
                    'rgba(255,99,132,1)',
                @endforeach
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

</script>
