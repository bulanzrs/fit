$(document).ready(function(){

    $('.group-slider').slick({
        dots: false,
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false
    });

});
