function initMap(a, d) {
        var c=L.map("mapid").setView([a, d], 13);
        L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibGltMjQ4MTI4NHRlc3QiLCJhIjoiY2ppd253Ymo0MDcyeTNybDlxY3h3dDVqbSJ9.Yib5cHheLWp53IfGfNzosw", {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>', maxZoom: 18, id: "mapbox.streets", accessToken: "pk.eyJ1IjoibGltMjQ4MTI4NHRlc3QiLCJhIjoiY2ppd253Ymo0MDcyeTNybDlxY3h3dDVqbSJ9.Yib5cHheLWp53IfGfNzosw"
        }
    ).addTo(c);
    var b=L.marker([a, d]).addTo(c)
}

$(document).ready(function(){
    initMap(51.505, -0.09);
});
