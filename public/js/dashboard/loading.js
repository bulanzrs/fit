$(document).ready(function(){
    $(document).on('click','.edit-btn',function(){

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var id = $(this).val();

        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: 'loading/'+ id,
            success: function (data) {
                console.log(data);
                $('.edit-note').val(data.note);
                $('.edit-status').val(data.status);
                $('.edit-status').change();
            }
        });
    })
})
