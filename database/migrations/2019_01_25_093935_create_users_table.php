<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('image_url')->nullable();
            $table->string('remember_token')->nullable();
            $table->unsignedInteger('role_id');
            $table->unsignedInteger('gender')->nullable();
            $table->integer('age')->nullable();
            $table->unsignedInteger('status')->default(1);
            $table->string('meta')->nullable();

            $table->integer('point')->unsigned()->default(0);
            $table->double('exp')->unsigned()->default(0);
            $table->integer('level')->unsigned()->default(1);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('role_id')
                ->references('id')->on('role')
                ->onDelete('restrict');
        });

        //Create default admin account
        DB::table('users')->insert([
            'name' => 'Superadmin',
            'email' => 'superuser@example.com',
            'password' => Hash::make(env('ADMIN_PASSWORD', 'Qwe123!@#')),
            'role_id' => 1,
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
        ]);

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
