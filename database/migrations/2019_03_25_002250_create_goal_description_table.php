<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::disableForeignKeyConstraints();
        Schema::create('goal_description', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class');
            $table->string('name');
            $table->string('meta')->nullable();
        });

        DB::table('role')->insert([
            [
                'class' => 'default',
                'name' => 'no goal',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'No Goal (Just for fun)'
                    ]
                ])
            ],
            [
                'class' => 'weight',
                'name' => 'lose weight',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Lose Weight'
                    ]
                ])
            ],
            [
                'class' => 'weight',
                'name' => 'gain weight',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Gain Weight'
                    ]
                ])
            ],
            [
                'class' => 'fit',
                'name' => 'get fitter',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Get Fitter'
                    ]
                ])
            ]
        ]);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal_description');
    }
}
