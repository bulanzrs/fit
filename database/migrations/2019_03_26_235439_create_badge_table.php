<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badge', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('image_url')->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('condition_id')->nullable();
            $table->double('condition_value')->nullable();
            $table->unsignedInteger('reward_id')->nullable();
            $table->double('reward_value')->nullable();
            $table->string('meta')->nullable();
            $table->timestamps();

            $table->foreign('condition_id')
                ->references('id')->on('condition')
                ->onDelete('cascade');
            $table->foreign('reward_id')
                ->references('id')->on('reward')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badge');
    }
}
