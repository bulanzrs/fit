<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condition', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class');
            $table->string('name');
            $table->string('meta')->nullable();
        });

        DB::table('condition')->insert([
            [
                'class' => 'social',
                'name' => 'group_visit',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Group Visit'
                    ]
                ])
            ],
            [
                'class' => 'level',
                'name' => 'Level reached',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Level'
                    ]
                ])
            ],
            [
                'class' => 'step',
                'name' => 'Total step walked ',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Total Step Walked'
                    ]
                ])
            ],
            [
                'class' => 'step',
                'name' => 'Total step walked in one day',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Total Step Walked Within Time'
                    ]
                ])
            ],
            [
                'class' => 'achievement',
                'name' => 'Number of achievement unlocked',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Achievement Unlocked'
                    ]
                ])
            ],
            [
                'class' => 'avatar',
                'name' => 'Number of avatar unlocked',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Avatar Unlocked'
                    ]
                ])
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condition');
    }
}
