<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward', function (Blueprint $table) {
            $table->increments('id');
            $table->string('class');
            $table->string('name');
            $table->string('meta')->nullable();
        });

        DB::table('reward')->insert([
            [
                'class' => 'point',
                'name' => 'Gain Point',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Gain Point'
                    ]
                ])
            ],
            [
                'class' => 'exp',
                'name' => 'Gain EXP',
                'meta' => json_encode([
                    'display' => [
                        'en' => 'Gain EXP'
                    ]
                ])
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward');
    }
}
