<?php

namespace App;

use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const SUPERADMIN = 1;
    const ADMIN =  2;
    const MEMBER =  3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','gender','age','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Function to get EXP progress
     * Every level increase 100 EXP
     */
    public function getExp()
    {
        $level = Auth::user()->level;
        $totalEXP = $level * 100;
        return (Auth::user()->exp / $totalEXP * 100 );
    }


    //Function to check if user has group
    public function hasGroup()
    {
        if(Auth::user()->groups->count())
            return true;
        return false;
    }


    //Relationship with group
    public function groups()
    {
        return $this->belongsToMany('App\Group','user_group','user_id','group_id');
    }

    //Relationship with attribute
    public function attributes()
    {
        return $this->belongsToMany('App\Attribute','user_tracking','user_id','attribute_id');
    }
}
