<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $table = 'goal';
    protected $guarded = ['id'];


    /*****************************
            Relationship
    *****************************/

    //Relationship with user
    public function users()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

}
