<?php

namespace App\Http\Controllers\App;

use Auth;
use App\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //Index page
    public function index()
    {
        //If not logged in redirect to landing page
        if(Auth::guest())
        {
            return view('app.page.landing');
        }

        $groups = Group::orderBy('created_at', 'desc')->take(15)->get();
        return view('app.page.homepage', compact('groups'));
    }
}
