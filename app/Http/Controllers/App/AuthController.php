<?php

namespace App\Http\Controllers\App;

use App\User;
use App\Role;
use App\Goal;

use Auth;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{

    use AuthenticatesUsers;


    /**
    * Where to redirect users after login.
    *
    * @var string
    */
    public function redirectTo(){
        //Login & gain EXP
        Controller::gainEXP(Controller::LOGIN_EXP);
        return '/';
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'captcha' => 'captcha'
        ]);
    }

    /**
    * Default login form
    */
    public function showLoginForm()
    {
        return view('app.page.login');
    }


    //Register form
    public function showRegisterForm()
    {
        return view('app.page.register');
    }


    //Register
    public function register(Request $request){

        $email = $request->email;
        $check = User::where('email',$email)->first();

        //Check email is registered
        if($check)
             return redirect('/register')->with('err', 'Email already registed.');

        //Create user
        $input['name'] = $request->name;
        $input['email'] = $email;
        $input['age'] = $request->age;
        $input['gender'] = $request->gender;
        $input['weight'] = $request->weight;
        $input['height'] = $request->height;
        $input['role_id'] = Role::USER();
        $input['password'] = Hash::make($request->pass);
        $user = User::create($input);
        $goalInput['goal_id'] = $request->goal;
        $goalInput['user_id'] = $user->id;
        $goalInput['progress'] = 0;
        Goal::create($goalInput);


       return redirect('/')->with('success', 'Account Created, please login');
    }

    //Manually validate authentication
    public function authenticate(Request $request)
    {
        $password = $request->input('password');
        $name = $request->input('name');

        if (Auth::attempt(['name' => $name, 'password' => $password]) )
        {
            return redirect()->intended('/');
        }
        return redirect()->back()->with('err', trans('lang.login_invalid'));

    }

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
    }
}
