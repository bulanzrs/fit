<?php

namespace App\Http\Controllers\App;

use App\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrackingController extends Controller
{
    //Index page
    public function index()
    {
        return view('app.page.track');
    }


    //Function to create new tracking record
    public function create(Request $request)
    {
        $track= Attribute::create($request->all());

        //Update pivot
        Auth::user()->attributes()->attach($track->id);

        //Daily track gain EXP
        Controller::gainEXP(Controller::TRACK_EXP);

        return response()->json(['success'=>true,'message'=>'Track created','body'=>$track]);
    }
}
