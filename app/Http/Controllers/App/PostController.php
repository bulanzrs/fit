<?php

namespace App\Http\Controllers\App;

use App\Group;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    //Create post
    public function createPost(Request $request)
    {
        $group = Group::where('reference_name',$request->reference_name)->first();

        //Create post
        $post = new Post;
        $post->content = $request->content;
        $post->owner_id = getUserID();
        $post->group_id = $group->id;
        $post->save();

        //Create post & gain EXP
        Controller::gainEXP(Controller::CREATE_POST_EXP);

        return redirect()->back()->with('success', "Post Created.");
    }
}
