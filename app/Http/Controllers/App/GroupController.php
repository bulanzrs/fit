<?php

namespace App\Http\Controllers\App;

use Auth;
use App\Group;
use App\UserGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    //Index page
    public function index()
    {
        $groups = Group::orderBy('created_at', 'desc')->take(15)->get();
        return view('app.page.group.index',compact('groups'));
    }


    //Create group
    public function create(Request $request)
    {
        //Check group name
        if(Group::where('name',$request->name)->first())
            return redirect()->back()->with('err', "Group name has been used.");

        //Generate reference name
        do{
            $reference = uniqid();
            $check = Group::where('reference_name',$reference)->first();
        }while($check);

        //Create new group
        $group = new Group;
        $group->name = $request->name;
        $group->reference_name = $reference;
        $group->owner_id = getUserID();
        $group->image_url = Group::DEFAULT_IMAGE;
        $group->description = $request->description;
        $group->privacy_type = $request->privacy;
        $group->activity_type = $request->activity;
        $group->save();

        //Create user group pivot record
        Auth::user()->groups()->attach($group->id);

        //Create group & gain EXP
        Controller::gainEXP(Controller::CREATE_GROUP_EXP);

        return redirect("/group/$reference");
    }


    //View group
    public function show($id)
    {
        $group = Group::where('reference_name',$id)->first();
        $posts = $group->posts;

        //Check if group exists
        if(!$group)
            abort(404);

        //Visit group & gain EXP
        if(!$group->isOwner())
            Controller::gainEXP(Controller::VISIT_GROUP_EXP);

        return view('app.page.group.show',compact('group','id','posts'));
    }


    //Search group
    public function search(Request $request)
    {
        $searchQuery = $request->search;
        $groups = Group::where('privacy_type','0');

        $groups = $groups->where(function ($query) use ($searchQuery) {
                    $query->where('name', 'like', "%$searchQuery%")
                          ->orWhere('description', 'like', "%$searchQuery%");
        });
        $groups = $groups->orderBy('created_at','DESC')->get();

        return view('app.page.group.search',compact('groups'));
    }


    //update group : group managemnet setting
    public function update(Request $request)
    {
        $group = Group::findOrFail($request->groupID);
        $group->update([
            'description' => $request->description,
            'name' => $request->name
        ]);
        return redirect()->back()->with('success','Group updated');
    }


    //Function to get group dettails
    public function get($id)
    {
        return Group::findOrFail($id);
    }

    //Join group
    public function join(Request $request)
    {
        //Join group attach function
        $groupID = $request->groupID;
        $group = Group::findOrFail($groupID);
        Auth::user()->groups()->attach($groupID);

        //Create group & gain EXP
        Controller::gainEXP(Controller::JOIN_GROUP_EXP);

        return redirect("/group/$group->reference_name")->with('success','You have join the group');
    }


    //Create avatar
    public function createAvatar(Request $request)
    {
        $path = $request->file('avatar')->storeAs(
              'avatars', $request->file('avatar')->getClientOriginalName()
        );

        $input = [
            'description' => $request->description,
            'title' => $request->title,
            'image_url' => $path,
            'condition_id' => $request->condition_id,
            'condition_value' => $request->condition_value,
            'reward_id' => $request->reward_id,
            'reward_value' => $request->reward_value
        ];

        Avatar::create($input);
        return redirect()->back()->with('success', "Avatar Created");
    }


    //Create badge
    public function createBadge(Request $request)
    {
        $path = $request->file('badge')->storeAs(
              'badges', $request->file('badge')->getClientOriginalName()
        );

        $input = [
            'description' => $request->description,
            'title' => $request->title,
            'image_url' => $path,
            'condition_id' => $request->condition_id,
            'condition_value' => $request->condition_value,
            'reward_id' => $request->reward_id,
            'reward_value' => $request->reward_value
        ];

        Badge::create($input);
        return redirect()->back()->with('success', "Badge Created");
    }
}
