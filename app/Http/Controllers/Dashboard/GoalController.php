<?php

namespace App\Http\Controllers\Dashboard;

use App\Goal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoalController extends Controller
{
    //Goal page
    public function index(Request $request){

        $records = new Goal;

        //Filter
        $query = $request->input('query');

        if($query)
            $records = $records->where('achievement', 'like', '%'.$query.'%');

        $records = $records->orderBy('created_at','desc')->paginate(30);

        return view('dashboard.page.goal',compact('records','query'));
    }


    //Get goal info
    public function getGoal($id)
    {
        $record = Goal::findOrFail($id);
        return $record;
    }

    //Create Goal
    public function create(Request $request)
    {
        $input['name'] = $request->name;
        Goal::create($input);
        return redirect()->back()->with('success', "Goal Created");
    }



    //Delete Goal
    public function delete(Request $request)
    {
        $id = $request->deleteID;
        $record = Goal::find($id)->delete();
        return back()->with( ['success' => "Goal Deleted"]);
    }



    //Update Goal
    public function update(Request $request)
    {
        $id = $request->editID;
        $record = Goal::findOrFail($id);
        $input['name'] = $request->card;
        $record->update($input);

        return back()->with( ['success' => "Goal Updated"  ]);
    }

}
