<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    //User page
    public function index(Request $request){

        //Filter
        $searchQuery = $request->input('query');
        $records = User::where('role_id',User::MEMBER);

        if($searchQuery)
            $records = $records->where(function ($query) use ($searchQuery) {
                        $query->where('name', 'like', "%$searchQuery%")
                              ->orWhere('email', 'like', "%$searchQuery%");
                });

        $records = $records->orderBy('created_at','desc')->paginate(30);
        return view('dashboard.page.user',compact('searchQuery','records'));
    }


    //Deactivate user



    //Activate user
}
