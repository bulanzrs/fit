<?php

namespace App\Http\Controllers\Dashboard;

use App\Badge;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BadgeController extends Controller
{
    //Badge page
    public function index(Request $request){

        $records = new Badge;
        $conditionArr = Controller::getConditionArr();
        $rewardArr = Controller::getRewardArr();

        //Filter
        $query = $request->input('query');

        if($query)
            $records = $records->where('title', 'like', '%'.$query.'%');

        $records = $records->orderBy('created_at','desc')->paginate(30);

        return view('dashboard.page.badge',compact('records','query','conditionArr','rewardArr'));
    }


    //Get achievement info
    public function getBadge($id)
    {
        $record = Badge::findOrFail($id);
        return $record;
    }


    //Create Badge
    public function create(Request $request)
    {
        $path = $request->file('badge')->storeAs(
              'badges', $request->file('badge')->getClientOriginalName()
        );

        $input = [
            'description' => $request->description,
            'title' => $request->title,
            'image_url' => $path,
            'condition_id' => $request->condition_id,
            'condition_value' => $request->condition_value,
            'reward_id' => $request->reward_id,
            'reward_value' => $request->reward_value
        ];

        Badge::create($input);
        return redirect()->back()->with('success', "Badge Created");
    }


    //Delete Badge
    public function delete(Request $request)
    {
        $id = $request->deleteID;
        $record = Badge::find($id)->delete();
        return back()->with( ['success' => "Badge Deleted"]);
    }


    //Update Badge
    public function update(Request $request)
    {
        $id = $request->editID;
        $record = Badge::findOrFail($id);
        $input['name'] = $request->card;
        $record->update($input);

        return back()->with( ['success' => "Badge Updated"  ]);
    }


}
