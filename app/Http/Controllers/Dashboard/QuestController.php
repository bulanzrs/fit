<?php

namespace App\Http\Controllers\Dashboard;

use App\Quest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestController extends Controller
{
    //Quest page
    public function index(Request $request){

        $records = new Quest;
        $conditionArr = Controller::getConditionArr();
        $rewardArr = Controller::getRewardArr();

        //Filter
        $query = $request->input('query');

        if($query)
            $records = $records->where('title', 'like', '%'.$query.'%');

        $records = $records->orderBy('created_at','desc')->paginate(30);

        return view('dashboard.page.quest',compact('records','query','conditionArr','rewardArr'));
    }



    //Get quest info
    public function getQuest($id)
    {
        $record = Quest::findOrFail($id);
        return $record;
    }


    //Create Quest
    public function create(Request $request)
    {
        $path = $request->file('quest')->storeAs(
              'quests', $request->file('quest')->getClientOriginalName()
        );

        $input = [
            'description' => $request->description,
            'title' => $request->title,
            'image_url' => $path,
            'condition_id' => $request->condition_id,
            'condition_value' => $request->condition_value,
            'reward_id' => $request->reward_id,
            'reward_value' => $request->reward_value
        ];

        Quest::create($input);
        return redirect()->back()->with('success', "Quest Created");
    }


    //Delete Quest
    public function delete(Request $request)
    {
        $id = $request->deleteID;
        $record = Quest::find($id)->delete();
        return back()->with( ['success' => "Quest Deleted"]);
    }


    //Update Quest
    public function update(Request $request)
    {
        $id = $request->editID;
        $record = Quest::findOrFail($id);
        $input['name'] = $request->card;
        $record->update($input);

        return back()->with( ['success' => "Quest Updated"  ]);
    }

}
