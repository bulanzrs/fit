<?php

namespace App\Http\Controllers\Dashboard;

use App\Avatar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AvatarController extends Controller
{
    //Avatar page
    public function index(Request $request){

        $records = new Avatar;
        $conditionArr = Controller::getConditionArr();
        $rewardArr = Controller::getRewardArr();

        //Filter
        $query = $request->input('query');

        if($query)
            $records = $records->where('title', 'like', '%'.$query.'%');

        $records = $records->orderBy('created_at','desc')->paginate(30);

        return view('dashboard.page.avatar',compact('records','query','conditionArr','rewardArr'));
    }



    //Get avatar info
    public function getAvatar($id)
    {
        $record = Avatar::findOrFail($id);
        return $record;
    }


    //Create Avatar
    public function create(Request $request)
    {
        $path = $request->file('avatar')->storeAs(
              'avatars', $request->file('avatar')->getClientOriginalName()
        );

        $input = [
            'description' => $request->description,
            'title' => $request->title,
            'image_url' => $path,
            'condition_id' => $request->condition_id,
            'condition_value' => $request->condition_value,
            'reward_id' => $request->reward_id,
            'reward_value' => $request->reward_value
        ];

        Avatar::create($input);
        return redirect()->back()->with('success', "Avatar Created");
    }


    //Delete Avatar
    public function delete(Request $request)
    {
        $id = $request->deleteID;
        $record = Avatar::find($id)->delete();
        return back()->with( ['success' => "Avatar Deleted"]);
    }


    //Update Avatar
    public function update(Request $request)
    {
        $id = $request->editID;
        $record = Avatar::findOrFail($id);
        $input['name'] = $request->card;
        $record->update($input);

        return back()->with( ['success' => "Avatar Updated"  ]);
    }

}
