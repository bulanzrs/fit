<?php

namespace App\Http\Controllers;

use App\Condition;
use App\Reward;
use App\User;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //EXP const
    const CREATE_GROUP_EXP = 1;
    const JOIN_GROUP_EXP = 2;
    const CREATE_POST_EXP = 1;
    const LOGIN_EXP = 5;
    const VISIT_GROUP_EXP = 1;
    const TRACK_EXP = 5;

    //Get Condition Array
    public static function getConditionArr(){
        return Condition::pluck('name','id')->toArray();
    }


    //Get Reward Array
    public static function getRewardArr(){
        return Reward::pluck('name','id')->toArray();
    }

    public static function gainEXP($exp , $userID = null)
    {
        if(!$userID)
            $userID =getUserID();

        $user = User::findOrFail($userID);
        $user->increment('exp',$exp);

        //Check level up
        if($user->exp >= ($user->level * 100))
            $user->increment('level',1);

    }


}
