<?php

namespace App\Http\Middleware;

use Closure;

class CheckSuperadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isSuperAdmin()) {
            abort(404);
            return false;
         }

        return $next($request);
    }
}
