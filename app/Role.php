<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';

    public static function USER(){
        return Role::where('name','user')->first()->id;
    }
}
