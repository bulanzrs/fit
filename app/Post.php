<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $guarded = ['id'];


    //Function to get owner name
    public function getOwner()
    {
        return $this->users->name;
    }


    //Relationship with user
    public function users()
    {
        return $this->belongsTo('App\User','owner_id','id');
    }
}
