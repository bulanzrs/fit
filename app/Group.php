<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $guarded = ['id'];

    const DEFAULT_IMAGE = '/img/icon/group.png';

    public function isOwner()
    {
        return ($this->owner_id == Auth::user()->id);
    }

    //Relationship with post
    public function posts()
    {
        return $this->hasMany('App\Post','group_id');
    }

    //Relationship with post
    public function users()
    {
        return $this->belongsToMany('App\User','user_group','group_id','user_id');
    }

    //Check if group has current user
    public function hasUser(){
        return $this->users->contains(getUserID());
    }

}
