<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $table = 'badge';
    protected $guarded = ['id'];

    /**
    * Relationship method for accessing the condition
    */
    public function conditions()
    {
        return $this->belongsTo('App\Condition','condition_id','id');
    }


    /**
    * Relationship method for accessing the reward
    */
    public function rewards()
    {
        return $this->belongsTo('App\Reward','reward_id','id');
    }
}
